export interface Restaurant {
    name: string;
    rooms: Room[];
    tables: Table[];
}

export interface Room {
    UID:string;
    name: string;
    tables: Table[];
}

export interface Table{
    UID:string;
    uid_room:string;
    name:string;
    state: 'open' | 'closed';

}