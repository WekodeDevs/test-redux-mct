import { HomeState, home_inital_state } from "../modules/home/store/home.state";

// import * as AppActions from "./app.actions";
export interface AppState {
    home: HomeState
}

export const appInitialState: AppState = {
    home: home_inital_state
}

export const selectAppState = (state: AppState) => state;