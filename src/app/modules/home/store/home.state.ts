import { Room } from "src/app/models/Entities";

export interface HomeState{
    rooms: Room[];
    selectedRoom: Room
}

export const home_inital_state: HomeState = {
    rooms:[],
    selectedRoom: null
}