import { createAction, props } from "@ngrx/store"
import { Restaurant, Room, Table } from "src/app/models/Entities"


export const HomeActions = {
    LOAD_RESTAURANT: '[Home] Load restaurant',
    LOAD_ROOMS: '[Home] Load rooms',
    LOAD_TABLES: '[Home] Load tables',

    SELECT_ROOM: '[Home] Select room'
}

export const loadRestaurant = createAction(HomeActions.LOAD_RESTAURANT, props<{restaurant: Restaurant}>());
export const loadRooms = createAction(HomeActions.LOAD_ROOMS, props<{rooms: Room[]}>());
export const loadTables = createAction(HomeActions.LOAD_TABLES, props<{tables: Table[]}>());
export const selectRoom = createAction(HomeActions.SELECT_ROOM, props<{room: Room}>());