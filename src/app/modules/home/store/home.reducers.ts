import { createReducer, on } from "@ngrx/store";
import { HomeState, home_inital_state } from "./home.state";
import * as HomeActions from './home.actions';
import { Room, Table } from "src/app/models/Entities";


export namespace HomeReducers {
    export const homeReducer = createReducer(home_inital_state,
        on(HomeActions.loadRestaurant, (state, { restaurant }): HomeState => {
            const rooms: Room[] = [...restaurant.rooms];

            restaurant.tables.forEach(table => {
                const tableRoom: Room = rooms.find(r => r.UID === table.uid_room);

                if(!tableRoom.tables){
                    tableRoom.tables = [];
                }

                tableRoom.tables.push(table);
            });


            return {
                ...state,
                rooms
            };
        }),
        on(HomeActions.loadRooms, (state, { rooms }): HomeState => {
            const newRooms: Room[] = rooms.map(room => {
                const oldRoom: Room = state.rooms.find(r => r.UID === room.UID);

                if(!oldRoom){
                    return {
                        ...room,
                        tables: []
                    }
                } else {
                    return {
                        ...room,
                        tables: oldRoom.tables
                    }
                }
            })

            return {
                ...state,
                rooms: newRooms
            };
        }),
        on(HomeActions.loadTables, (state, { tables }): HomeState => {

            let newRooms: Room[] = state.rooms.map(r => ({...r, tables: []}));

            tables.forEach(table => newRooms.find(nr => nr.UID === table.uid_room)?.tables.push(table));

            return {
                ...state,
                rooms: newRooms
            };
        }),
        on(HomeActions.selectRoom, (state, { room }): HomeState => {

            return {
                ...state,
                selectedRoom: room
            };
        })
    );
}