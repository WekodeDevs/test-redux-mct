import { createFeatureSelector, createSelector } from "@ngrx/store";
import { HomeState } from "./home.state";

export namespace HomeSelectors {
    export const getHomeState = createFeatureSelector<HomeState>('home');

    export const getRooms = createSelector(getHomeState, (state: HomeState) => state.rooms);
    export const getSelectedRoom = createSelector(getHomeState, (state: HomeState) => state.selectedRoom);

}