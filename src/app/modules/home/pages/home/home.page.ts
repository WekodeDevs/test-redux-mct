import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AngularFirestore, AngularFirestoreCollection, DocumentReference, CollectionReference, DocumentData, QuerySnapshot, AngularFirestoreCollectionGroup } from '@angular/fire/firestore';
import { AppState } from 'src/app/store/app.state';
import { Restaurant, Room, Table } from 'src/app/models/Entities';
import { map, take } from 'rxjs/operators';
import { loadRestaurant, loadRooms, loadTables, selectRoom } from '../../store/home.actions';
import { Observable } from 'rxjs';
import { HomeSelectors } from '../../store/home.selectors';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HomePage implements OnInit {

  public rooms$: Observable<Room[]>;
  public selectedRoom$: Observable<Room>;

  constructor(
    private afs: AngularFirestore,
    private store: Store<AppState>
  ) {
    this.afs.collection<Room>('/rooms', ref => ref.where('UID_RESTAURANT', '==', 'yPM9f1mqBrn7NIWlJuou')).snapshotChanges().pipe(
      map(roomsFs => {
        console.log(roomsFs);
        const rooms: Room[] = roomsFs.map( r => {
          const id: string = r.payload.doc.id;
          let data: Room = r.payload.doc.data();
          return { UID: id, ...data}
        });

        this.store.dispatch(loadRooms({ rooms }));
        // if(restaurantFs){
        //   const restaurant: Restaurant = {
        //     ...restaurantFs.payload.doc.data()
        //   };
        //   debugger;
  
        //   this.store.dispatch(loadRestaurant({ restaurant }));
        // }
      })
    ).subscribe();

    this.afs.collection<Table>('/tables', ref => ref.where('UID_RESTAURANT', '==', 'yPM9f1mqBrn7NIWlJuou')).snapshotChanges().pipe(
      map(tablesFs => {
        console.log(tablesFs);
        const tables: Table[] = tablesFs.map( r => {
          const id: string = r.payload.doc.id;
          let data: Table = r.payload.doc.data();
          return { UID: id, ...data}
        });
        
        this.store.dispatch(loadTables({ tables }));
        // if(restaurantFs){
        //   const restaurant: Restaurant = {
        //     ...restaurantFs.payload.doc.data()
        //   };
        //   debugger;
  
        //   this.store.dispatch(loadRestaurant({ restaurant }));
        // }
      })
    ).subscribe();
  }

  ngOnInit() {
    this.rooms$ = this.store.select(HomeSelectors.getRooms);
    this.selectedRoom$ = this.store.select(HomeSelectors.getSelectedRoom);
  }

  selectRoom(room: Room){
    this.store.dispatch(selectRoom({room}));
  }

}
